package concretesolutions.desafioandroid;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.sudarmuthu.android.taskmanager.util.ObjectSerializer;

import concretesolutions.desafioandroid.cep.CEPModel;

public class CEPApplication extends Application{

	private History history;
	private String key = "HISTORY";
	public CEPApplication() {
		history = new History();
	}

	public History getHistory() {
		return history;
	}

	public void saveHistory (Context context) {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
        Editor editor = prefs.edit();
        
        try {
            editor.putString(key, ObjectSerializer.serialize(history.getHistory()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void loadHistory (Context context) {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);

	        try {
	            ArrayList<CEPModel> obj = new ArrayList<CEPModel>();
				history.setHistory( ((ArrayList<CEPModel>) ObjectSerializer.deserialize(
	            				prefs.getString(key, ObjectSerializer.serialize(obj)))));
	        } catch (IOException e) {
	            e.printStackTrace();
	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        }
	}
}
