package concretesolutions.desafioandroid.cep.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

public class CepEditText extends EditText {

	public CepEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {

		addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				rebuildText();

			}
		});

	}

	private void rebuildText() {
		String in = getText().toString();

		String text = in.replaceAll("[^(0-9)|-]", "");
		Log.i("Debug", in + " - > " + text);
		if (text.equals(in)) {
			if (text.contains("-")) {

				if ((text.lastIndexOf("-") == text.indexOf("-"))
						&& (text.indexOf("-") == 5)) {
					if (text.length() > 9) {
						text = text.substring(0, 9);
						setText(text);
						setSelection(text.length());
					}
				} else {
					text = text.replaceAll("-", "");
					if (text.length() > 4) {
						if (text.length() == 5) {
							text = text.concat("-");
						} else {
							Log.i("Debug", "Replace: " + text);
							String aux = text.substring(0, 5);
							String aux2 = text.substring(5, text.length());
							Log.i("Debug", "Replace: " + aux + "-" + aux2);
							text = aux + "-" + aux2;
						}

					}
					if (!text.equals("")) {
						setText(text);
						setSelection(text.length());
					}

				}

			} else {
				if (text.length() > 4) {
					if (text.length() == 5) {
						text = text.concat("-");
					} else {
						Log.i("Debug", "Replace: " + text);
						String aux = text.substring(0, 5);
						String aux2 = text.substring(5, text.length());
						Log.i("Debug", "Replace: " + aux + "-" + aux2);
						text = aux + "-" + aux2;
					}
					setText(text);
					setSelection(text.length());
				}
				
			}
		} else {
			setText(text);
			setSelection(text.length());
		}
	}
}
