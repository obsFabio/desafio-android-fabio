package concretesolutions.desafioandroid.cep.request;

public class InvalidCepExeception extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "Cep Inv�lido";
	}
	
}
