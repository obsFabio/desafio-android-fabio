package concretesolutions.desafioandroid.cep.request;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.annotation.SuppressLint;
import android.os.StrictMode;
import concretesolutions.desafioandroid.cep.CEPModel;

public class HttpCEPRequester {

	@SuppressLint("NewApi")
	public CEPModel execute(String cep) throws IOException, InvalidCepExeception {

		// allow network in the UI thread
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		cep = cep.replaceAll("-", "");
		String url = "http://correiosapi.apphb.com/cep/" + cep;

		String response = executeGet(url);

		CEPModel model = new CEPModel();
		model.setFromApiResponse(response);
		
		return model;
	}

	private String executeGet(String urlString) throws ClientProtocolException, IOException {

		HttpClient httpClient = new DefaultHttpClient();
		HttpGet get = new HttpGet(urlString);
		
		HttpResponse response = httpClient.execute(get);
		String ret = EntityUtils.toString(response.getEntity());
		
		return ret;
		
		
	}
	
}
