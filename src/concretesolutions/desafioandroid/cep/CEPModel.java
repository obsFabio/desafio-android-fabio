package concretesolutions.desafioandroid.cep;

import java.io.Serializable;

import concretesolutions.desafioandroid.cep.request.InvalidCepExeception;


import android.util.Log;

public class CEPModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String typeOfStreet = "";
	private String street = "";
	private String state = "";
	private String city = "";
	private String cep = "";
	private String neighborhood = "";

	public String getTypeOfStreet() {
		return typeOfStreet;
	}

	public void setTypeOfStreet(String typeOfStreet) {
		this.typeOfStreet = typeOfStreet;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public void setFromApiResponse(String response)
			throws InvalidCepExeception {

		if (response.contains("Endere�o n�o encontrado!")) {
			throw new InvalidCepExeception();
		}

		response = response.replace("\"","");
		response = response.replace("}","");
		response = response.replace("{","");

		Log.i("Debug", response);
		for (String s : response.split(",")) {
			String pieces[] = s.split(":");
			String key = pieces[0];

			if (pieces.length > 1) {
				Log.i("Debug", "Pedacos " + key + " -> " + pieces[1]);

				if (key.equals("tipoDeLogradouro")) {
					setTypeOfStreet(pieces[1]);
				} else if (key.equals("logradouro")) {
					setStreet(pieces[1]);
				} else if (key.equals("estado")) {
					setState(pieces[1]);
				} else if (key.equals("cidade")) {
					setCity(pieces[1]);
				} else if (key.equals("cep")) {
					setCep(pieces[1]);
				} else if (key.equals("bairro")) {
					setNeighborhood(pieces[1]);
				}
			}
		}

	}

}
