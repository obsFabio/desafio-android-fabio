package concretesolutions.desafioandroid.screens;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import concretesolutions.desafioandroid.CEPApplication;
import concretesolutions.desafioandroid.R;
import concretesolutions.desafioandroid.cep.CEPModel;
import concretesolutions.desafioandroid.cep.request.HttpCEPRequester;
import concretesolutions.desafioandroid.cep.request.InvalidCepExeception;

public class SearchCEPActivity extends ActionBarActivity {

	private TextView cep_type_of_street, cep_state, cep_street, cep_cep,
			cep_city, cep_neighborhood;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_cep);

		CEPApplication app = (CEPApplication) getApplicationContext();
		app.loadHistory(getApplicationContext());

		Button button = (Button) findViewById(R.id.cep_search);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				HttpCEPRequester requester = new HttpCEPRequester();
				EditText cep_field = (EditText) findViewById(R.id.cep_field);
				CEPModel cepModel;
				try {
					cepModel = requester
							.execute(cep_field.getText().toString());
					showCep(cepModel);

					CEPApplication app = (CEPApplication) getApplicationContext();
					app.getHistory().addCep(cepModel);

				} catch (InvalidCepExeception e) {
					Toast.makeText(getApplicationContext(), e.toString(),
							Toast.LENGTH_LONG).show();
				} catch (Exception e) {
					Toast.makeText(getApplicationContext(),
							"Impossivel acessar os correios",
							Toast.LENGTH_LONG).show();
				}
			}
		});

		cep_type_of_street = (TextView) findViewById(R.id.cep_type_of_street);
		cep_state = (TextView) findViewById(R.id.cep_state);
		cep_street = (TextView) findViewById(R.id.cep_street);
		cep_cep = (TextView) findViewById(R.id.cep_cep);
		cep_city = (TextView) findViewById(R.id.cep_city);
		cep_neighborhood = (TextView) findViewById(R.id.cep_neighborhood);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.search_cep, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.cepHistory) {
			openPopup(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@SuppressLint("NewApi")
	private void openPopup(final Activity context) {
		
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;
		
		int popupWidth = (int)(((float)width)*0.8);
		int popupHeight = (int)(((float)height)*0.2);

		// Inflate the popup_layout.xml
		LinearLayout viewGroup = (LinearLayout) context
				.findViewById(R.id.popup);
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = layoutInflater.inflate(R.layout.popup_layout, viewGroup);

		// Creating the PopupWindow
		final PopupWindow popup = new PopupWindow(context);
		popup.setContentView(layout);
		popup.setWidth(popupWidth);
		popup.setHeight(popupHeight);
		popup.setFocusable(true);

		// Some offset to align the popup a bit to the right, and a bit down,
		// relative to button's position.
		int OFFSET_X = (int)(((float)width)*0.1);
		int OFFSET_Y = (int)(((float)height)*0.3);

		// Clear the default translucent background
		popup.setBackgroundDrawable( getResources().getDrawable(R.drawable.white) );

		// Displaying the popup at the specified location, + offsets.
		View mainLayout  = context.findViewById(R.id.searche_layout);
		popup.showAtLocation(mainLayout, Gravity.NO_GRAVITY, OFFSET_X, OFFSET_Y);

		CEPApplication app = (CEPApplication) getApplicationContext();
		
		
		Spinner spinner = (Spinner) layout.findViewById(R.id.popup_list);
		ArrayList<String> elements = app.getHistory().getCEPList();

		for ( String s: elements)
			Log.i("Debug", s);
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, elements);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);

		
		Button button = (Button) layout.findViewById(R.id.popup_button);
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Spinner spinner = (Spinner) popup.getContentView().findViewById(R.id.popup_list);
				CEPApplication app = (CEPApplication) getApplicationContext();
				String cep = (String) spinner.getSelectedItem();
				showCep(app.getHistory().getByCEP(cep));
				Log.i("Debug","cep " + cep);
				popup.dismiss();
			}
		});
		
		
		
		

	}

	protected void onStart() {
		super.onStart();

		Log.i("Debug", "ConfigurationChanged");
		CEPApplication app = (CEPApplication) getApplicationContext();
		CEPModel cep = app.getHistory().lastCep();

		if (cep != null) {
			showCep(cep);
		}

	}

	public void showCep(CEPModel cepModel) {
		cep_type_of_street.setText(cepModel.getTypeOfStreet());
		cep_street.setText(cepModel.getStreet());
		cep_state.setText(cepModel.getState());
		cep_neighborhood.setText(cepModel.getNeighborhood());
		cep_city.setText(cepModel.getCity());
		cep_cep.setText(cepModel.getCep());
	}

	@Override
	protected void onDestroy() {
		CEPApplication app = (CEPApplication) getApplicationContext();
		app.saveHistory(getApplicationContext());
		super.onDestroy();
	}
}
