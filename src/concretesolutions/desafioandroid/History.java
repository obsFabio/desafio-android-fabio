package concretesolutions.desafioandroid;

import java.util.ArrayList;

import concretesolutions.desafioandroid.cep.CEPModel;

import android.util.Log;

public class History {

	private ArrayList<CEPModel> history;
	
	public History() {
		setHistory(new ArrayList<CEPModel>());
	}
	
	public CEPModel lastCep () {
		Log.i("Debug", "last " + getHistory().isEmpty());
		if ( getHistory().isEmpty())
			return null;
		
		return getHistory().get(getHistory().size()-1);
	}
	
	public void addCep ( CEPModel cep ){

		if ( getByCEP(cep.getCep()) == null)
			getHistory().add(cep);
		
	}
	
	public ArrayList<CEPModel> consult (){
		return getHistory();
	}

	public ArrayList<CEPModel> getHistory() {
		return history;
	}

	public void setHistory(ArrayList<CEPModel> history) {
		this.history = history;
	}
	
	public ArrayList<String> getCEPList () {
		ArrayList<String> list = new ArrayList<String>();
		
		for ( CEPModel cep: history){
			list.add(cep.getCep());
		}
		return list;
	}

	public CEPModel getByCEP(String cep) {
		for ( CEPModel cm : history){
			if ( cm.getCep().equals(cep))
				return cm;
		}
		return null;
	}
}
