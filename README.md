# Desafio Concrete Solutions Android  
  
### Criar um aplicativo Android de consulta de CEP

- O aplicativo deverá consultar um serviço REST público de CEP (passaremos URL e documentação).
- A interface deverá conter um campo para digitar o CEP, um botão de consulta e um botão de visualizar consultas anteriores.
- A lista de consultas anteriores deverá ser apresentada em um popup
- O aplicativo deverá ter tratamento para os casos de erro (sem conexão, CEP inválido, serviço indisponível)
- Ganha pontos se:


a) **[+3]** o aplicativo for desenvolvido com AndroidAnnotations  
b) **[+5]** o aplicativo for desenvolvido com Maven  
c) **[+2]** o aplicativo suportar mudanças de orientação sem erros (portrait e landscape)  
d) **[+1]** o campo de CEP possuir máscara  
e) **[+1 | +2]** o aplicativo persistir a busca de CEPs (ganha mais pontos se não utilizar banco de dados)  
f) **[+3]** criar um projeto de testes automatizados  
g) **[+1]** usar boas práticas de desenvolvimento Java (estrutura do projeto, desacoplamento e etc)

### Processo de submissão  

O candidato deverá implementar a solução e enviar um pull request para este repositório com a solução.

### URL e Documentação de um serviço de consulta de CEP RESTful:
Documentação da API: http://correiosapi.apphb.com 
Exemplo de uso da API: http://correiosapi.apphb.com/cep/76873274